<?php
session_start();
require_once 'modelo/Persona.php';
require_once 'modelo/Administrador.php';

require_once 'modelo/Facultad.php';
require_once 'modelo/Proyecto.php';
require_once 'modelo/Tipo.php';
require_once 'modelo/Usuario.php';
require_once 'modelo/Celador.php';
require_once 'modelo/Gerente.php';

require_once 'modelo/Color.php';
require_once 'modelo/Marca.php';
require_once 'modelo/Identificacion.php';
require_once 'modelo/Transporte.php';
require_once 'modelo/Parqueadero.php';

?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="cssLogin/styles.css">
    


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
   
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
   	<script charset="utf-8">
        $(function () { 
        	$("[data-toggle='tooltip']").tooltip(); 
        });
    </script>
    <title>Parqueadero UDistrital</title>
</head> 

<body>


<?php    
    if(isset($_GET["pid"])){
        $pid = base64_decode($_GET["pid"]);
        if(isset($_GET["nos"]) || !isset($_GET["nos"]) && $_SESSION["id"]!=""){
            include $pid;
        }else{
        header("Location: index.php");
        }
    }else{
        $_SESSION["id"]="";
        include "presentacion/Login.php";
    }
    
?>


</body>