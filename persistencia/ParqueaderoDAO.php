<?php

class ParqueaderoDAO{

    private $idparqueadero;
    private $numero;
    private $estado;
    private $puestosOcupados;
    private $puestosMaximos;

    function ParqueaderoDAO($idparqueadero= "" , $numero= "", $estado= "", $puestosOcupados= "", $puestosMaximos= ""){        
        $this -> idparqueadero = $idparqueadero;
        $this -> numero = $numero;
        $this -> estado = $estado;
        $this -> puestosOcupados = $puestosOcupados;
        $this -> puestosMaximos = $puestosMaximos;
    }
    

    function registrar(){
        return  "insert into parqueadero (idparqueadero, numero, puestosOcupados, puestosMaximos)
                values ('" . $this-> idparqueadero . "', '" . $this-> numero . "', '".$this -> puestosOcupados . "','" .$this -> puestosMaximos . "')";
    }

    function actualizar(){
        return "update parqueadero set 
                numero = '" . $this -> numero . "', 
                puestosOcupados = '" . $this -> puestosOcupados . "',
                puestosMaximos = '" . $this -> puestosMaximos . "',

                where idparqueadero=" . $this -> idparqueadero;
    }

    function actualizarEstado(){
        return "update parqueadero set
                estado = '" . $this -> estado . "'
                where idparqueadero=" . $this -> idparqueadero;
    }
    
    function consultar() {
        return "select numero, puestosOcupados, puestosMaximos
                from parqueadero
                where 	idparqueadero =" . $this -> idparqueadero;
    }

    function consultarTodoss(){
        return "select idparqueadero, numero, estado, puestosOcupados, puestosMaximos
                from parqueadero
                order by idparqueadero";
    }
}

?>