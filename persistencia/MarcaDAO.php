<?php

class MarcaDAO{
    
    private $id;
    private $nombre;
    private $idTipo;

    function MarcaDAO($id = "", $nombre = "", $idTipo=""){
        
        $this->id = $id;
        $this->nombre = $nombre;
        $this->idTipo = $idTipo;
    }
    
    function consultar() {
        return "select nombre, idTipo
                from marca
                where idmarca =" . $this -> id;
    }

    function consultarMotosTodos(){
        return "select idmarca, nombre, idTipo
                from marca
                where idTipo = 2
                order by idmarca";
    }
    function consultarBicisTodos(){
        return "select idmarca, nombre, idTipo
                from marca
                where idTipo = 3
                order by idmarca";
    }
}

?>
