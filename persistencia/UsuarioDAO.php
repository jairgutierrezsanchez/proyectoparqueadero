<?php

class UsuarioDAO {

    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $password;
    private $codigoEstudiantil;
    private $direccion;
    private $telefono;
    private $foto;
    private $estado;
    private $numeroID;
    private $idProyecto;
    private $idTipoIdentificacion;

    function UsuarioDAO($id= "", $nombre= "", $apellido= "", $correo= "", $password= "", $codigoEstudiantil= "", $direccion= "", $telefono= "", 
                        $foto= "", $estado= "", $numeroID= "", $idProyecto= "", $idTipoIdentificacion= ""){

        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->correo = $correo;
        $this->password = $password;
        $this->codigoEstudiantil = $codigoEstudiantil;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
        $this->foto = $foto;
        $this->estado = $estado;
        $this->numeroID = $numeroID;
        $this->idProyecto = $idProyecto;
        $this->idTipoIdentificacion = $idTipoIdentificacion;
    }

    function registrar(){
        return  "insert into usuario (nombre, apellido, correo, password, idProyecto, idTipoIdentificacion, estado)
                values ('" . $this->nombre . "', '" . $this->apellido . "', 
                        '" . $this->correo . "', '" . md5($this->password) . "', 
                        '" . $this->idProyecto . "', '" . $this->idTipoIdentificacion ."',
                        '" . $this->estado . "')";
    }

    function autenticar(){
        return "select idusuario, estado
                from usuario 
                where correo = '" . $this -> correo . "' and password = '" . md5($this-> password) . "'";
    }

   
    function actualizar(){
        return "update usuario set 
                nombre = '" . $this -> nombre . "',
                apellido='" . $this -> apellido . "',
                codigoEstudiantil='" . $this -> codigoEstudiantil . "',
                direccion='" . $this -> direccion . "',
                telefono='" . $this -> telefono . "',
                numeroID='" . $this -> numeroID . "',
                idProyecto='" . $this -> idProyecto . "',
                idTipoIdentificacion='" . $this -> idTipoIdentificacion . "' 
                where idusuario=" . $this -> id;
    }
    
    function actualizarFoto(){
        return "update usuario set
                foto = '" . $this -> foto . "'
                where idusuario=" . $this -> id;
    }

    function actualizarEstado($estado){
        return "update usuario set
                estado = '" . $estado . "'
                where idusuario=" . $this -> id;
    }
    
    function consultar() {
        return "select nombre, apellido, correo, codigoEstudiantil, direccion, telefono, numeroID, idProyecto, idTipoIdentificacion
                from usuario
                where idusuario =" . $this -> id;
    }

    function crear() {
        return  "insert into usuario (nombre, apellido, correo, password, codigoEstudiantil, 
                                        direccion, telefono, numeroID, idProyecto, idTipoIdentificacion)
                values ('" . $this->nombre . "', '" . $this->apellido . "', '" . $this->correo . "', '" . md5($this -> password) . "','".$this -> codigoEstudiantil . "', 
                        '".$this -> direccion . "', '".$this -> telefono . "', '".$this -> numeroID . "','".$this -> idProyecto . "', '" .$this -> idTipoIdentificacion ."')";
    }

    function eliminar() {
        return "delete usuario 
                from usuario
                where idusuario =" . $this -> id;
    }
        
    function existeCorreo(){
        return "select idusuario 
                from usuario
                where correo = '" . $this->correo . "'";
    }

    function consultarTodos(){	
        return "select idusuario, nombre, apellido, correo, codigoEstudiantil, foto, estado, idProyecto
                from usuario
                order by idusuario";
    }
    
    function consultarEstado(){
        return "select estado
                from usuario
                where idusuario = " . $this -> id;
    }
}


        
?>