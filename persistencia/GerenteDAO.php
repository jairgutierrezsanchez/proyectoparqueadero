<?php
class GerenteDAO{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $password;
    private $numeroID;
    private $idtipoIdentificacion;
    private $idFacultad;
    
    function GerenteDAO($id, $nombre, $apellido, $correo, $password, $numeroID, $idtipoIdentificacion, $idFacultad){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> password = $password;
        $this -> numeroID = $numeroID;
        $this -> idtipoIdentificacion = $idtipoIdentificacion;
        $this -> idFacultad = $idFacultad;
    }

    function registrar(){
        return  "insert into gerente (nombre, apellido, correo, password, numeroID, idtipoIdentificacion, idFacultad)
                values ('" . $this->nombre . "', '" . $this->apellido . "', 
                        '" . $this->correo . "', '" . md5($this->password) . "', 
                        '" . $this->numeroID . "', '" . $this->idtipoIdentificacion ."',
                        '" . $this->idFacultad . "')";
    }

    function autenticar(){
        return "select idgerente
                from gerente 
                where correo = '" . $this -> correo . "' and password = '" . md5($this-> password) . "'";
    }

    function actualizar(){
        return "update gerente set 
                nombre = '" . $this -> nombre . "',
                apellido='" . $this -> apellido . "',
                codigoEstudiantil='" . $this -> codigoEstudiantil . "',
                direccion='" . $this -> direccion . "',
                telefono='" . $this -> telefono . "',
                numeroID='" . $this -> numeroID . "',
                idTipoIdentificacion='" . $this -> idTipoIdentificacion . "',
                idFacultad='" . $this -> idFacultad . "'
                
                where idgerente=" . $this -> id;
    }

    function existeCorreo(){
        return "select idgerente 
                from gerente
                where correo = '" . $this->correo . "'";
    }

    function consultar() {
        return "select nombre, apellido, correo, numeroID, idTipoIdentificacion, idFacultad
                from gerente
                where idgerente =" . $this -> id;
    }

    function consultarTodos(){	
        return "select idgerente, nombre, apellido, correo, numeroID, idTipoIdentificacion, idFacultad
                from gerente
                order by idgerente";
    }
}
           