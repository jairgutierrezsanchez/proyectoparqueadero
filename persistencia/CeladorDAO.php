<?php

class CeladorDAO{
    
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $password;
    private $direccion;
    private $telefono;
    private $numeroID;
    private $foto;
    private $estado;
    private $idTipoIdentificacion;
    private $idFacultad;
    private $idParqueadero;

    function CeladorDAO($id = "", $nombre = "", $apellido = "", $correo = "", $password = "", $direccion  ="", $telefono = "", $numeroID = "", $foto = "", $estado = "",
                        $idTipoIdentificacion = "", $idFacultad = "", $idParqueadero = ""){
        
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->correo = $correo;
        $this->password = $password;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
        $this->numeroID = $numeroID;
        $this->foto = $foto;
        $this->estado = $estado;
        $this->idTipoIdentificacion = $idTipoIdentificacion;
        $this->idFacultad = $idFacultad;
        $this->idParqueadero = $idParqueadero;
    }

    function registrar(){
        return  "insert into celador (nombre, apellido, correo, password, estado, idTipoIdentificacion, idFacultad)
                values ('" . $this->nombre . "', '" . $this->apellido . "', 
                        '" . $this->correo . "', '" . md5($this->password) . "',
                        '" . $this->estado . "', '" . $this->idTipoIdentificacion . "', 
                        '" . $this->idFacultad . "')";
    }

    function autenticar(){
        return "select idcelador
                from celador 
                where correo = '" . $this -> correo . "' and password = '" . md5($this-> password) . "'";
    }

    function actualizar(){
        return "update celador set 
                nombre = '" . $this -> nombre . "',
                apellido= '" . $this -> apellido . "', 
                direccion= '" . $this -> direccion . "',
                telefono= '" . $this -> telefono . "',
                numeroID= '" . $this -> numeroID . "',
                idTipoIdentificacion= '" . $this -> idTipoIdentificacion . "'
                where idcelador=" . $this -> id;	
    }
    
    function actualizarFoto(){
        return "update celador set
                foto = '" . $this -> foto . "'
                where idcelador=" . $this -> id;
    }

    function actualizarEstado($estado){
        return "update celador set
                estado = '" . $estado . "'
                where idcelador=" . $this -> id;
    }
    
    function consultar() { 
        return "select nombre, apellido, correo, direccion, telefono, numeroID, idTipoIdentificacion
                from celador
                where 	idcelador =" . $this -> id;
    }

    function existeCorreo(){
        return "select idcelador 
                from celador
                where correo = '" . $this->correo . "'";
    }

    function consultarTodos(){
        return "select idcelador, nombre, apellido, numeroID, foto, estado
                from celador
                order by idcelador";
    }
    function consultarEstado(){
        return "select estado
                from celador
                where idcelador = " . $this -> id;
    }


	
}

?>