<?php 
// require 'logica/Persona.php';
// require 'logica/Paciente.php';
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();

$error = -1;
$nombre = "";
$apellido = "";
$correo = "";
$password = "";

if(isset($_POST["registrar"])){
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $password = $_POST["password"];
    
    $usuario = new Usuario("", $nombre, $apellido, $correo, $password, "", "", "", "",2,"",1,1);
    if(!$usuario -> existeCorreo()){
        $usuario -> registrar();
        $error = 0;
    }else{
        $error = 1;
    }
}

include 'presentacion/menuAdministrador.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssRegistro/styleRegistro.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
  <div class="fondoRegistro">
    <div class="cardFormulario">
                <div class="card">
                  <div class="card-title">
                    <h1>CREAR USUARIO</h1>
                  </div>
                    <div class="card-body">
                    <?php 
                      if($error == 0){
                      ?>
                      <div class="alert alert-success" role="alert">
                        Usuario registrado exitosamente.
                      </div>
                      <?php } else if($error == 1) { ?>
                      <div class="alert alert-danger" role="alert">
                        El correo <?php echo $correo; ?> ya existe
                      </div>
                    <?php } ?>
                        <form class="formlario" action=<?php echo "index.php?pid=" .base64_encode("presentacion/Administrador/crearUsuario-admin.php")."&nos=true" ?> method="post">
                            <div class="mb-3">
                              <input type="text" name="nombre" class="form-control" id="exampleInputEmail1"  placeholder="nombre" value="<?php echo $nombre; ?>">
                            </div>
                            <div class="mb-3">
                              <input type="text" name="apellido" class="form-control" id="exampleInputEmail1"  placeholder="apellido" value="<?php echo $apellido; ?>">
                            </div>
                            <div class="mb-3">
                              <input type="email" name="correo" name="nombre" class="form-control" id="exampleInputEmail1"  placeholder="correo institucional" value="<?php echo $correo; ?>">
                            </div>
                            <div class="mb-3">
                              <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="contraseña" value="<?php echo $password; ?>">
                            </div>
                            <button type="submit" name="registrar" class="boton mb-2">Registrar</button>
                            <div class="hipervinculo">
                              <a href="presentacion/Administrador/pagSeleccionUsuario-admin.php">Volver</a>	
                            </div>
                          </form>
                    </div>
                  </div>
            </div>
                
            
</div>

    
</body>
</html>