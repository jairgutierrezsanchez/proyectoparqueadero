<?php
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
$proyecto = new Proyecto();
$identificacion = new Identificacion();

include 'presentacion/usuario/menuUsuario.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Perfil Usuario</title>
</head>
<body>
<h1 class="titulosUsuario">PERFIL USUARIO</h1>
    <div class="contorno mt-3">
        <div class="card contorno-cardPerfiles">
            <div class="row g-0">
                <div class="col-md-4 mt-1 contorno-cardPerfiles-imagen">
                    <img src="./img/profile.png" alt="">
                </div>
                <div class="col-md-8">
                    <div class="card-body contorno-cardPerfiles-cuerpoCard">
                        <h5 class="card-title contorno-cardPerfiles-cuerpoCard-nombreTitulo"><?php echo $usuario -> getNombre() . " " . $usuario -> getApellido() ?></h5>
                        <div class="card-text contorno-cardPerfiles-cuerpoCard-infoUsuario">
                            <div>
                                <?php echo "Correo: ". $usuario -> getCorreo() ?> 
                            </div> 
                            <div>
                                <?php echo "Codigo: ". $usuario -> getCodigoEstudiantil() ?>
                            </div> 
                            <div>
                                <?php $proyecto ->setId($usuario -> getIdProyecto()) ?>
                                <?php  $proyecto -> consultar() ?> 
                                <?php echo "Proyecto: ". $proyecto -> getNombre() ?>
                            </div>
                            <div>
                                <?php $identificacion -> setId($usuario -> getIdTipoIdentificacion()) ?>
                                <?php  $identificacion -> consultar() ?> 
                                <?php echo "Tipo documento: ". $identificacion -> getNombreTipo() ?>
                            </div>
                            <div>
                                <?php echo "Número documento: ". $usuario -> getNumeroID() ?>
                            </div>
                        </div>
                    </div>
                    <div class="contorno-cardPerfiles-botonCard mt-5">
                        <a class="contorno-cardPerfiles-botonCard-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/actualizarUsuario.php")?>">Actualizar datos</a>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</body>
</html>