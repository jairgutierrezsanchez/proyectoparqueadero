<?php
$exito = "";
if($_SESSION["rol"]=="usuario"){
    $usuario = new Usuario($_SESSION["id"]);
    $usuario -> consultar();
    include "presentacion/usuario/menuUsuario.php";
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
    include "presentacion/menuAdministrador.php";
}

$usuario = new Usuario($_GET["idusuario"]);
$usuario->consultar();
if (isset($_POST["actualizar"])) {
    // recibimos los datos de la imagen
    $nombre_foto = $_FILES['foto']['name'];
    $tipo_foto = $_FILES['foto']['type'];
    $tam_foto = $_FILES['foto']['size'];
    if ($tam_foto <= 300000) {
        if (strlen($nombre_foto) <= 45) {
            if ($tipo_foto == "image/png" || $tipo_foto == "image/jpeg" || $tipo_foto == "image/jpg") {
                if ($paciente->getFoto()) {
                    unlink("C:/xampp/htdocs/proyectoParqueadero/proyectoparqueadero/img/" . $usuario->getFoto());
                }
                // ruta de la carpeta destino en el servidor
                $carpeta_destino = $_SERVER['DOCUMENT_ROOT'] . '/proyectoParqueadero/proyectoparqueadero/img/';
                // movemos la imagen de la carpeta temporal al directorio escogido
                move_uploaded_file($_FILES['foto']['tmp_name'], $carpeta_destino . $nombre_foto);

                $usuario = new Usuario($_GET["idusuario"], "", "", "", "", "", "", "", "", $nombre_foto); // Crear el atributo foto en usuario
                $usuario->actualizarFoto();
            } else {
                $exito = "El tipo de la foto solo puede ser png,jpeg y jpg";
            }
        } else {
            $exito = "El nombre de de la
						foto es muy largo.";
        }
    } else {
        $exito = "El tamano de la
						foto es muy grande.";
    }
}
include 'presentacion/menuAdministrador.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Actualizar Usuario</title>
</head>
<body>
    <h1 class="titulosUsuario">ACTUALIZAR FOTO USUARIO</h1>
    <div class="container">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">

			<div class="card">

				<div class="card-header bg-primary text-white">Actualizar Foto
                usuario</div>
				<div class="card-img-top">
					<img src="/IPSUD/fotos/<?php echo $usuario->getFoto()?>" height="200px" />
					<div class="card-body">
						<p class="card-text">foto del usuario</p>
					</div>
				</div>
				<div class="card-body">
				
						<?php
    if (isset($_POST["actualizar"])) {
        ?>
        	<?php if($exito!=""){?>
        	<div class="alert alert-danger" role="alert"><?php echo $exito ?></div>
        	    
        	<?php }else{?>
        	    <div class="alert alert-success" role="alert">foto
						actualizada</div>
        	<?php }?>		
						<?php } ?>
						
					<form
						action=<?php echo "index.php?pid=" . base64_encode("presentacion/usuario/actualizarFotoUsuario.php")."&idUsuario=".$_GET["idUsuario"] ?>
						method="post" enctype="multipart/form-data">
						<div class="form-group">
							<input type="file" name="foto" size="30" class="form-control"
								placeholder="Foto" required="required">
						</div>
						<button type="submit" name="actualizar" class="btn btn-primary">Actualizar</button>
					</form>
				</div>
			</div>
		</div>

	</div>

</div>
            


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>       
</body>
</html>
