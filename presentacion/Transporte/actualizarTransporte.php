<?php 
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();

$error = -1;
$serial = "";
$modelo = "";
$foto = "";
$fotoPropiedad = "";
$descripcion = "";
$idcolor = "";
$idmarca = "";
$idtipo = "";
$idusuario = "";

if(isset($_POST["actualizar"])){
    $serial = $_POST["serial"];
    $modelo = $_POST["modelo"];
    $foto = $_POST["foto"];
    $fotoPropiedad = $_POST["fotoPropiedad"];
    $descripcion = $_POST["descripcion"];
    $idcolor = $_POST["idcolor"];
    $idmarca = $_POST["idmarca"];
    $idtipo = $_POST["idtipo"];
    $idusuario = $_POST["idusuario"];
    
    $transporte = new Transporte("", $serial, $modelo, $foto, $fotoPropiedad, $descripcion, 0, $idcolor, $idmarca, $idtipo, $idusuario);
    if(!$transporte -> existeSerial()){
        $transporte -> registrar();
        $error = 0;
    }else{
        $error = 1;
    }
}
include 'presentacion/usuario/menuUsuario.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Crear Espacio</title>
</head>
<body>
    
<h2 class="mt-5">CREAR ESPACIOS *<span>el usuario puede tener como máximo dos medios de transporte registrados</span></h2>

<div class="cardInfoUsuario mt-3">
        <div class="card cardActualizar">
                <div class="card-body cUA"> 
                    <?php if (isset($_POST["actualizar"])) { ?>
                        <div class="alert alert-success" role="alert">Usuario actualizado exitosamente.</div>						
                    <?php } ?>
                    <div class="row mt-2">
                        <div class="form-group col-md-3">
                            <label for="inputState">Tipo de medio de transporte</label>
                                <select id="inputState" class="form-control" name="transporte">
                                            <?php
                                                $tipo = new Tipo();
                                                $tipos = $tipo -> consultarTodos();
                                                foreach ($tipos as $t) {
                                                echo "<option value='" . $t->getId() . "'>" . $t->getTipo() . "</option>";
                                                }
                                            ?>
                                        </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputState">Marca</label>
                                    <select id="inputState" class="form-control" name="marca">
                                        <?php
                                            $marca = new Marca();
                                            $marcas = $marca -> consultarTodos();
                                            foreach ($marcas as $m) {
                                            echo "<option value='" . $m->getId() . "'>" . $m->getNombre() . "</option>";
                                            }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="inputState">Color</label>
                                    <select id="inputState" class="form-control" name="color">
                                        <?php
                                            $color = new Color();
                                            $colores = $color -> consultarTodos();
                                            foreach ($colores as $c) {
                                            echo "<option value='" . $c->getId() . "'>" . $c->getNombre() . "</option>";
                                            }
                                        ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="form-group col-md-4">
                            <label for="inputState">Modelo</label>
                            <i class="fas fa-toggle-on"></i>
                            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Escribe el modelo">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputState">Serial</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Escribe el serial o placa">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputState">Descripción</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Escribe una breve descripción sobre de los detalles de tu medio">
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="form-group col-md-4">
                            <label for="inputState">Subir foto del medio</label>
                            <input type="file" name="foto" size="30" class="form-control" placeholder="Foto" required="required">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputState">Foto de carta de propiedad</label>
                            <input type="file" name="foto" size="30" class="form-control" placeholder="Foto" required="required">
                        </div>
                    </div>
                    <div class="container mt-2 mb-3">
                        <div class="col-md-12 d-flex justify-content-end">
                            <a class="boton" href="presentacion/Transporte/actualizarTransporte.php">Actualizar</a>
                        </div>
                        <div class="col-md-12 d-flex justify-content-end">
                            <button class="boton" name="actualizar">Registrar</button>
                        </div>
                    </div>
               </div>
            </div>
        </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> 
</body>
</html>