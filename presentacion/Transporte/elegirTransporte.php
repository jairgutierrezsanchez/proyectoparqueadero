<?php 
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
include "presentacion/usuario/menuUsuario.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Perfil Usuario</title>
</head>
<body>
<h1>POR FAVOR, ELIJA EL MEDIO DE TRANSPORTE QUE DESEA REGISTRAR</h1>
<div class="containerCardMotoBici">
    <a  href="index.php?pid=<?php echo base64_encode("presentacion/transporte/crearTransporte.php")."&tipo=Bicicleta"?>">
        <div class="card containerCardMotoBici-elegir mr-5">
            <i class="fas fa-bicycle"></i>  
            <div class="card-footer bg-transparent ">Registrar Bicicleta</div>
        </div>
    </a>
        
    <a href="index.php?pid=<?php echo base64_encode("presentacion/transporte/crearTransporte.php")."&tipo=Moto"?>">
        <div class="card containerCardMotoBici-elegir ml-5">
            <i class="fas fa-motorcycle"></i>  
            <div class="card-footer bg-transparent ">Registrar Moto</div>
        </div>-*
    </a>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>