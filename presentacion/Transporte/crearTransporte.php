<?php 
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
$t = new Transporte("","","","","","","","",(($_GET["tipo"]=="Moto")?3:2),$_SESSION["id"]);
$t -> consultarFotosVehiculo();

include "presentacion/usuario/menuUsuario.php";

$error = -1;
$serial = "";
$modelo = "";
$descripcion = "";
$idcolor = "";
$idtipo =  $_GET["tipo"];
$idmarca = "";
   

if(isset($_POST["registrar"])){
    $serial = $_POST["serial"];
    $modelo = $_POST["modelo"];
    $descripcion = $_POST["descripcion"];
    $idcolor = $_POST["idcolor"];
    $idmarca = $_POST["idmarca"];
    
    if($_FILES["type"] == "jpg"){
        $rutaServidor = "imagenes/" . date("Ymdhis") . ".jpg";
        echo "entro";
    }else{
        $rutaServidor = "imagenes/" . date("Ymdhis") . ".png";
    }
    if($t-> getFotoTransporte() != ""){
        unlink($t -> getFotoTransporte());
    }     
    $rutaLocal = $_FILES["imagenVehiculo"]["tmp_name"];
    copy($rutaLocal, $rutaServidor);
    $transporte = new Transporte("", $serial, $modelo, $rutaServidor,$rutaLocal, $descripcion, 0, $idcolor, $idtipo, $_SESSION["id"],$idmarca, 0);
    if(!$transporte -> existeSerial()){
        $transporte -> registrar();
        $error = 0;
    }else{
        $error = 1;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Crear Espacio</title>
</head>
<body>
    
<h1 id="tituloTransporte"class="mt-1"><?php echo "Crear ". $_GET["tipo"];?></h1>

<div class="crearTraan">
        <?php 
            if($error == 0){
        ?>
        <div class="alert alert-success" role="alert">
            Medio de transporte registrado exitosamente.
        </div>
        <?php } else if($error == 1) { ?>
        <div class="alert alert-danger" role="alert">
        <?php echo (($_GET["tipo"]=="Moto")?"La placa ".$serial. " ya existe":"El serial ".$serial. " ya existe")?>
        </div>
        <?php } ?>
    <form  action= <?php echo "index.php?pid=" . base64_encode("presentacion/Transporte/crearTransporte.php")."&tipo=" . $_GET["tipo"]?> method="post" enctype="multipart/form-data">
        <div class="row mt-2">
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="inputState">Marca</label>
                <select id="inputState" class="form-control" name="idmarca">
                    <?php
                        $marca = new Marca();
                        $marcas = (($_GET["tipo"]=="Bicicleta")?$marca -> consultarMotosTodos():$marca -> consultarBicisTodos());
                        foreach ($marcas as $m) {
                            echo "<option value='" . $m->getId() . "'>" . $m->getNombre() . "</option>";
                        }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="inputState">Color</label>
                <select id="inputState" class="form-control" name="idcolor">
                    <?php
                        $color = new Color();
                        $colores = $color -> consultarTodos();
                        foreach ($colores as $c) {
                            echo "<option value='" . $c->getIdC() . "'>" . $c->getColor() . "</option>";
                        }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="inputState"><?php echo (($_GET["tipo"]=="Moto")?"Placa":"Serial")?></label>
                <input type="text" name="serial" class="form-control" id="exampleFormControlInput1" placeholder="Escribe el serial o placa">
            </div>
        </div>
        <div class="row mt-2">
            
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="inputState">Modelo</label>
                <input type="text" name="modelo" class="form-control" id="exampleFormControlInput1" placeholder="Escribe el modelo">
            </div>
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="inputState">Descripción</label>
                <input type="text" name="descripcion" class="form-control" id="exampleFormControlInput1" placeholder="Descripción sobre tu medio">
           </div>
             <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="inputState">Foto <?php $_GET["tipo"];?></label>
                <input type="file" name="imagenVehiculo" class="form-control" id="exampleFormControlInput1" placeholder="Escribe el modelo" required = "required">
            </div>
        </div>
        <div class="row mt-2">
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="formFile">Foto Carta de propiedad</label>
                <input type="file" name="fotoCartaDePropiedad" class="form-control" id="exampleFormControlInput1" placeholder="Descripci�n sobre tu medio" required = "required">
            </div>
        </div>
        <div class="container d-flex justify-content-space-between mt-4 mb-3">
            <div class="col-md-9">
                <button class="botonCrearTransporte" name="registrar">Registrar</button>
            </div>
            
        </div>

    </form>
</div>





    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> 
</body>
</html>