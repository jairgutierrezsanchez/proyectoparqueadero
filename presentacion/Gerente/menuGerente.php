<?php 
$gerente = new Gerente($_SESSION["id"]);
$gerente -> consultar();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Document</title>
</head>
<body> 
	<div class="barraNavegacion">
		<nav class="navbar navbar-expand-lg">
			<a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/sesionCelador.php")?>"><i class="fas fa-home" style="color: white; margin-left:15"></i></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">

					<li class="nav-item dropdown">
						<a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/celador/pagSeleccionUsuario-celador.php")?>" >Usuarios</a>
					</li>	
					<li class="nav-item dropdown">
						<a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/celador/pagSeleccionTransporte-celador.php")?>" >Espacios</a>
					</li>
					<li class="nav-item dropdown"><a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/gerente/perfilGerente.php")?>" >Perfil</a></li>
				</ul>
				<div class="barraNavegacion-logout">
					<ul class="navbar-nav">
						<li class="nav-item t">
							<a class="nav-link"><i class="fas fa-hammer" style="color: white; margin-left: 15px;"></i> <?php echo $gerente -> getNombre() . " " . $gerente -> getApellido() ?></a>
						</li>
						<li class="nav-item t">
							<a class="nav-link" href="index.php?sesion=0">Cerrar Sesión</a>
						</li>
					</ul>
				</div>

			</div>
		</nav>
	</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>
</html>