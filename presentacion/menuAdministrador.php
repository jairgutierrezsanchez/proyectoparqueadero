<?php 
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar();

?>



<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Document</title>
</head>
<body>
	
<div class="barraNavegacion">
<nav class="navbar navbar-expand-lg">
	<a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php")?>"><i class="fas fa-home" style="color: white; margin-left:15"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<!--<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Consultar Usuarios
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/crearUsuario.php")?>">Crear</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/consultarUsuario.php")?>">Consultar</a>
					
				</div>
				</li>-->
			<li class="nav-item dropdown">
				<a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/pagSeleccionUsuario.php")?>" >Estudiantes</a>
			</li>

			<li class="nav-item dropdown">
				<a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/Administrador/pagSeleccionEspacios-admin.php")?>" >Medios de Trasnporte Registrados</a>
			</li>

			<li class="nav-item dropdown">
				<a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/celador/pagSeleccionCelador.php")?>" >Celador</a>
			</li>

			<li class="nav-item dropdown">
				<a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/perfilAdmin.php")?>" >Perfil</a>
			</li>

		</ul>
		<div class="barraNavegacion-logout">
	        <ul class="navbar-nav">
				<li class="nav-item t">
					<a class="nav-link"><i class="fas fa-user-circle"></i> <?php echo $administrador -> getNombre() . " " . $administrador -> getApellido() ?></a>
				</li>
				<li class="nav-item t">
            		<a class="nav-link" href="index.php">Cerrar Sesión</a>
            	</li>
          </ul>
        </div>

	</div>
</nav>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>