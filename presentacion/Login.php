<div class="containerr">
        <section class="izq">
            <div class="izq-fondoLogin">
                <img class="izq-fondoLogin-imagen" src="./img/fondoLogin.jpg" alt="">
            </div>
        </section>
        <section class="der">
            <div class="der-titulo">
                <h4>Parqueadero Universidad Distrital <br> Francisco José de Caldas</h4>
            </div>
            <div class="imagen" style="padding-left: 225px;">
               <img src="./img/descarga.png" alt="Fondo de bicicletas rodando">
            </div> 
            <div class="der-cardFormulario ">
                <div class="card mt-10">
                    <div class="card-body">
                        <?php
                        if(isset($_GET["error"])){
                            if($_GET["error"] == 1){
                                echo "<div class='alert alert-warning' role='alert'>";
                                echo "Credenciales invalidas";
                                echo "</div>";
                            }else if($_GET["error"] == 2){
                                echo "<div class='alert alert-danger' role='alert'>";
                                echo "Perfil Inhabilitado";
                                echo "</div>";
                            }
                        }
                        ?>
                        <form class="formlario" action= "index.php?pid=<?php echo base64_encode("presentacion/autenticar.php")?>&nos=true" method="post">
                            <div class="mb-3">
                              <input name="correo" type="email" class="form-control" id="Email" aria-describedby="emailHelp" placeholder="correo institucional">
                            </div>
                            <div class="mb-3">
                              <input name="password" type="password" class="form-control" id="Password" placeholder="contraseña">
                            </div>
                            <button type="submit" name="autenticar" class="boton mb-2">Ingresar</button>
                            <div class="letra"><h6>si aún no está registrado</h6></div>
                            <div class="hipervinculo">
                                    <a href=<?php echo "index.php?pid=" . base64_encode("presentacion/Registro.php") . "&nos=true" ?>>Regístrese Acá.</a> 
                                </div>
                          </form>
                    </div>
                  </div>
            </div>
        </section>
    </div>
</body>
