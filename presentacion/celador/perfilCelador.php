<?php
$celador = new Celador($_SESSION["id"]);
$celador -> consultar();
$identificacion = new Identificacion();

include 'presentacion/celador/menuCelador.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Perfil Celador</title>
</head>
<body>
<h1 class="titulosUsuario">PERFIL CELADOR</h1>
<div class="contorno mt-3">
        <div class="card contorno-cardPerfiles">
            <div class="row g-0">
                <div class="col-md-4 mt-1 contorno-cardPerfiles-imagen">
                    <img src="./img/profile.png" alt="">
                </div>
                <div class="col-md-8">
                    <div class="card-body contorno-cardPerfiles-cuerpoCard">
                        <h5 class="card-title contorno-cardPerfiles-cuerpoCard-nombreTitulo"><?php echo $celador -> getNombre() . " " . $celador -> getApellido() ?></h5>
                        <div class="card-text contorno-cardPerfiles-cuerpoCard-infoUsuario">
                            <div>
                                <?php echo "Correo: ". $celador -> getCorreo() ?> 
                            </div>
                            <div>
                                <?php $identificacion -> setId($celador -> getIdTipoIdentificacion()) ?>
                                <?php  $identificacion -> consultar() ?> 
                                <?php echo "Tipo documento: ". $identificacion -> getNombreTipo() ?>
                            </div>
                            <div>
                                <?php echo "Número documento: ". $celador -> getNumeroID() ?>
                            </div>
                        </div>
                    </div>
                    <div class="contorno-cardPerfiles-botonCard mt-5">
                        <a class="contorno-cardPerfiles-botonCard-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/celador/actualizarCelador.php")?>">Actualizar datos</a>
                    </div>    
                </div>
            </div>
        </div>
    </div>

</body>
</html>