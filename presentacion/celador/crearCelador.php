<?php
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();
include 'presentacion/menuAdministrador.php';

$error = -1;
$nombre = "";
$apellido = "";
$correo = "";
$password = "";

if(isset($_POST["crear"])){
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $password = $_POST["password"];

    $celador = new Celador("",	$nombre, $apellido, $correo, $password, "", "",	"",	"", 0, 1, 1, 1);
    if(!$celador -> existeCorreo()){
        $celador -> registrar();
        $error = 0;
    }else{
        $error = 1;
    }
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
    <h1 class="titulosUsuario">CREAR CELADOR</h1>
        <div class="crearUsuario mt-1">
            <?php 
                if($error == 0){
            ?>
            <div class="alert alert-success" role="alert">
                Celador registrado exitosamente.
            </div>
            <?php } else if($error == 1) { ?>
            <div class="alert alert-danger" role="alert">
                El correo <?php echo $correo; ?> ya existe
            </div>
            <?php } ?>
            <form action=<?php echo "index.php?pid=" .base64_encode("presentacion/celador/crearCelador.php")."&nos=true" ?> method="post">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label id="crearTrasnporte-label" for="inputState">Nombre</label>
                        <input type="text" name="nombre" class="form-control" id="exampleFormControlInput1" placeholder="Escribe tu nombre">
                    </div>
                    <div class="form-group col-md-6">
                        <label id="crearTrasnporte-label" for="inputState">Apellido</label>
                        <input type="text" name="apellido" class="form-control" id="exampleFormControlInput1" placeholder="Escribe tu apellido">
                    </div> 
                </div>
                <div class="row mt-3">   
                    <div class="form-group col-md-6">
                        <label id="crearTrasnporte-label" for="inputState">Correo</label>
                        <input type="email" name="correo" class="form-control" id="exampleFormControlInput1" placeholder="Escribe tu correo">
                    </div>
                    <div class="form-group col-md-6">
                        <label id="crearTrasnporte-label" for="inputState">Contraseña</label>
                        <input type="password" name="password" class="form-control" id="exampleFormControlInput1" placeholder="Escribe tu contraseña">
                    </div>
                </div>
                <div class="containerBotonCrear mt-4">
                    <button class="botonCrearUsuario" name="crear">Crear</button>
                </div>
            </form>
        </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>