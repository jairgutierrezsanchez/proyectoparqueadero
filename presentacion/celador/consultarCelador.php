<?php 
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar(); 
$celador = new Celador();
$celadores = $celador->consultarTodos();

include "presentacion/menuAdministrador.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Consultar Celador</title>
</head>
<body>
<div class="consultaCard">
		<h1 class="tituloConsulta mb-2">CONSULTAR CELADORES</h1>
		<div class="consultarr">
			<table class="table">
				<thead class="table-light"> 
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Nombre</th>
						<th scope="col">Apellido</th>
						<th scope="col">Número de Identificación</th>
						<th scope="col">Foto</th>
						<th scope="col">Estado</th>
						<th scope="col">Servicios</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($celadores as $c) {
							echo "<tr>";
							echo "<td>" . $c->getId() . "</td>";
							echo "<td>" . $c->getNombre() . "</td>";
							echo "<td>" . $c->getApellido() . "</td>";
							echo "<td>" . $c->getNumeroID() . "</td>";
							echo "<td>" . (($c->getFoto()!="")?"<img src='/IPSUD/fotos/" . $c->getFoto() . "' height='50px'>":"") . "</td>";
							echo "<td><span id='estado" .$c->getId() ."'  class='fas " . ($c->getEstado()==0?"fa-times-circle":"fa-check-circle") . "' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='" . ($c->getEstado()==0?"Inhabilitado":"Habilitado") . "' ></span>" . "</td>";
							echo "<td>" . "<a href='modalCelador.php?idCelador=" . $c->getId() . "' data-bs-toggle='modal' data-bs-target='#modalCelador' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Ver Detalles' > </span> </a>
										<a class='fas fa-pencil-ruler' href='index.php?pid=" . base64_encode("presentacion/Administrador/actualizarCelador-admin.php") . "&idcelador=" . $c->getId() . "' data-toggle='tooltip' data-placement='left' title='Actualizar'>  </a>"
						    .($c -> getEstado()<=1?" <a id='cambiarEstado" . $c->getId() . "' class='fas " .($c->getEstado()==0? "fa-user-check": "fa-user-times"). "' href='#' data-toggle='tooltip' data-placement='left'> </a>":"") .
						    "</td>"; 
							echo "</tr>";
						}
					echo "<caption>" . count($celadores) . " registros encontrados</caption>"?>
				</tbody>
			</table>
		</div>
	</div>
	

<!--SCRIPTS ADICIONALES-->
<div class="modal fade" id="modalCelador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>

<script type="text/javascript">
	<?php 
		foreach ($celadores as $c) {
			if($c -> getEstado() !=2){
				echo "$('#cambiarEstado" . $c -> getId() . "').click(function() {\n";
				echo "\tvar url = 'indexAjax.php?pid=" . base64_encode("presentacion/celador/editarEstadoCeladorAjax.php") . "&id=" . $c -> getId() . "';\n";
				echo "\t$('#cambiarEstado" . $c -> getId() . "').load(url);\n";
				echo "\tif($('#cambiarEstado" . $c -> getId() . "').attr('class') == 'fas fa-user-times'){\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('class', 'fas fa-user-check');\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('data-original-title', 'Habilitar');\n";
				echo "\t}else{\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('class', 'fas fa-user-times');\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('data-original-title', 'Desabilitar');\n";
				echo "\t}\n";
				echo "\tif($('#estado" . $c -> getId() . "').attr('class') == 'fas fa-times-circle'){\n";
				echo "\t\t$('#estado" . $c -> getId() . "').attr('class', 'fas fa-check-circle');\n";
				echo "\t}else{\n";
				echo "\t\t$('#estado" . $c -> getId() . "').attr('class', 'fas fa-times-circle');\n";
				echo "\t}\n";
				echo "});\n";

				
				        
			}
		}
	?>
</script>
	


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>