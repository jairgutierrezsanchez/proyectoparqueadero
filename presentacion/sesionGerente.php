<?php
$gerente = new Gerente($_SESSION['id']);
$gerente->consultar();
include 'presentacion/gerente/menuGerente.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Sesión Usuario</title>
</head>
<body>
<h1 class="titulosUsuario">Bienvenido, gerente: <?php echo $gerente -> getNombre() . " " . $gerente -> getApellido() ?> </h1>
</body>
</html>