<?php
require 'persistencia/ParqueaderoDAO.php';
require_once 'persistencia/Conexion.php';

class Parqueadero {
    private $idparqueadero;
    private $numero;
    private $estado;
    private $puestosOcupados;
    private $puestosMaximos;
    private $parqueaderoDAO;
    private $conexion;	
   
    public function getIdparqueadero(){
        return $this->idparqueadero;
    }

    public function getNumero(){
        return $this->numero;
    }

    public function getEstado(){
        return $this->estado;
    }

    public function getPuestosOcupados(){
        return $this->puestosOcupados;
    }

    public function getPuestosMaximos(){
        return $this->puestosMaximos;
    }

    public function getParqueaderoDAO(){
        return $this->parqueaderoDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }
   
    function Transporte($idparqueadero= "" , $numero= "", $estado= "", $puestosOcupados= "", $puestosMaximos= ""){
        $this -> idparqueadero = $idparqueadero;
        $this -> numero = $numero;
        $this -> estado = $estado;
        $this -> puestosOcupados = $puestosOcupados;
        $this -> puestosMaximos = $puestosMaximos;
        $this -> conexion = new Conexion();
        $this -> parqueaderoDAO = new ParqueaderoDAO($idparqueadero , $numero, $estado, $puestosOcupados, $puestosMaximos);

    }

    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
    function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO ->actualizar());
        $this -> conexion -> cerrar();
    }

    function actualizarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO ->actualizarEstado());
        $this -> conexion -> cerrar();
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> numero = $resultado[0];
        $this -> estado = $resultado[1];
        $this -> puestosOcupados = $resultado[2];
        $this -> puestosMaximos = $resultado[3];
        $this -> conexion -> cerrar();
    }
    
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Parqueadero($registro[0], $registro[1], $registro[2], $registro[3]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
}