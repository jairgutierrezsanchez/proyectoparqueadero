<?php
require 'persistencia/UsuarioDAO.php';
require_once 'persistencia/Conexion.php';

class Usuario extends Persona {
    private $codigoEstudiantil;
    private $estado;
    private $idProyecto;
    private $usuarioDAO;
    private $conexion;

    public function getCodigoEstudiantil(){
        return $this->codigoEstudiantil;
    }

    public function getEstado(){
        return $this->estado;
    }

    public function getIdproyecto(){
        return $this->idProyecto;
    }

    public function getUsuarioDAO(){
        return $this->usuarioDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }
	
    function Usuario ($id="", $nombre="", $apellido="", $correo="", $password="", $codigoEstudiantil="", $direccion="", $telefono="", 
                        $foto="", $estado="", $numeroID="", $idProyecto="", $idTipoIdentificacion=""){ 
        $this -> Persona($id , $nombre, $apellido, $correo, $password, $direccion, $telefono, $foto, $numeroID, $idTipoIdentificacion);
        $this -> codigoEstudiantil = $codigoEstudiantil;
        $this -> estado = $estado;
        $this -> idProyecto = $idProyecto;
        $this -> conexion = new Conexion();
        $this -> usuarioDAO = new UsuarioDAO($id, $nombre, $apellido, $correo, $password, $codigoEstudiantil, $direccion, $telefono, $foto, $estado, $numeroID, 
                                                $idProyecto, $idTipoIdentificacion);        
    }
    
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> registrar());
        $this -> conexion -> cerrar();
    }

    function autenticar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> usuarioDAO -> autenticar());
        if($this -> conexion -> numFilas() == 1){
            $registro = $this -> conexion -> extraer(); 
            $this -> id = $registro[0];
            $this -> estado = $registro[1];
            $this -> conexion -> cerrar();
            return true;
        }else{
            $this -> conexion -> cerrar();
            return false;
        }
    }
    
    function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO ->actualizar());
        $this -> conexion -> cerrar();
    }
    
    function actualizarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO ->actualizarFoto());
        $this -> conexion -> cerrar();
    }

    function actualizarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO ->actualizarEstado($estado));
        $this -> conexion -> cerrar();
    }
    
    function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> existeCorreo());
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return false;
        } else {
            $this -> conexion -> cerrar();
            return true;            
        }
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> codigoEstudiantil = $resultado[3];
        $this -> direccion = $resultado[4];
        $this -> telefono = $resultado[5];
        $this -> numeroID = $resultado[6];
        $this -> idProyecto = $resultado[7];
        $this -> idTipoIdentificacion = $resultado[8];
        $this -> conexion -> cerrar();
    }

    function crear(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> crear());
        $this -> conexion -> cerrar();
    }

    function eliminar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar ($this -> usuarioDAO -> eliminar());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Usuario($registro[0], $registro[1], $registro[2], $registro[3], "", $registro[4], "", "", $registro[5], $registro[6], "", $registro[7], "");
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;  
    }
    function consultarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarEstado());
        $resultado = $this -> conexion -> extraer();
            $this -> estado = $resultado[0];
    }
}