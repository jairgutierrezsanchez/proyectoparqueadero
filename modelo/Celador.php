<?php
require 'persistencia/CeladorDAO.php';
require_once 'persistencia/Conexion.php';

class Celador extends Persona {
    
    private $estado;
    private $idFacultad;
    private $idParqueadero;
    private $celadorDAO;
    private $conexion;	

    public function getEstado(){
        return $this->estado;
    }

    public function getIdFacultad(){
        return $this->idFacultad;
    }

    public function getIdParqueadero(){
        return $this->idParqueadero;
    }

    public function getCeladorDAO(){
        return $this->celadorDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }

    function Celador ($id="", $nombre="", $apellido="", $correo="", $password="",

    $direccion="", $telefono="", $numeroID="", $foto= "", $estado="", $idTipoIdentificacion="", $idFacultad="", $idParqueadero=""){ 

        $this -> Persona($id , $nombre, $apellido, $correo, $password, $direccion, $telefono, $foto, $numeroID, $idTipoIdentificacion);
        $this -> estado = $estado;
        $this -> idFacultad = $idFacultad;
        $this -> idParqueadero = $idParqueadero;

        $this -> conexion = new Conexion();
        $this -> celadorDAO = new CeladorDAO($id, $nombre, $apellido, $correo, $password, $direccion, $telefono, $numeroID, $foto, $estado, $idTipoIdentificacion, $idFacultad, $idParqueadero);        
    }
   
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> registrar());
        $this -> conexion -> cerrar();
    }

    function autenticar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> celadorDAO -> autenticar());
        if($this -> conexion -> numFilas() == 1){
            $registro = $this -> conexion -> extraer(); 
            $this -> id = $registro[0];
            $this -> conexion -> cerrar();
            return true;
        }else{
            $this -> conexion -> cerrar();
            return false;
        }
    }
    
    function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO ->actualizar());
        $this -> conexion -> cerrar();
    }
    
    function actualizarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO ->actualizarFoto());
        $this -> conexion -> cerrar();
    }

    function actualizarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO ->actualizarEstado($estado));
        $this -> conexion -> cerrar();
    }
    
    function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> existeCorreo());
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return false;
        } else {
            $this -> conexion -> cerrar();
            return true;            
        }
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> direccion = $resultado[3];
        $this -> telefono = $resultado[4];
        $this -> numeroID = $resultado[5];
        $this -> idTipoIdentificacion = $resultado[6];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Celador($registro[0], $registro[1], $registro[2], "", "", "", "",  
                                            $registro[3], $registro[4], $registro[5], "", "", "");
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    function consultarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> consultarEstado());
        $resultado = $this -> conexion -> extraer();
        $this -> estado = $resultado[0];
    }
    
}