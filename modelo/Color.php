<?php
require 'persistencia/ColorDAO.php';
require_once 'persistencia/Conexion.php';

class Color {
    private $idC;
    private $color;
    private $colorDAO;
    private $conexion;
    
    public function getIdC(){
        return $this->idC;
    }

    public function getColor(){
        return $this->color;
    }

    public function getColorDAO(){
        return $this->colorDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }

    function Color($idC="" , $color=""){
        $this -> idC = $idC;
        $this -> color = $color;
        $this -> conexion = new Conexion();
        $this -> colorDAO = new ColorDAO($idC, $color);        
    
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> colorDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> color = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> colorDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Color($registro[0], $registro[1]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
}